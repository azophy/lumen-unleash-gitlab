# Experiment on using Gitlab's Feature Flagging with Laravel Lumen

## Library used:
- [Laravel Lumen](https://lumen.laravel.com/).
- [Gitlab's Feature Flagging](https://docs.gitlab.com/ee/operations/feature_flags.html)
- [Laravel-Unleash package](https://github.com/mikefrancis/laravel-unleash)

## License

[MIT license](https://opensource.org/licenses/MIT).
